import urllib2
import json
import sqlite3
from HTMLParser import HTMLParser

def getNewToken():
	# getting token
	data = urllib2.urlopen("https://opentdb.com/api_token.php?command=request").read()
	jsonData = json.loads(data)
	token = jsonData['token']
	# resetting token
	urllib2.urlopen("https://opentdb.com/api_token.php?command=reset&token=" + token)
	return token

def getFiles(wFile):
	token = getNewToken() # getting token for non repetitive questions
	total_count = 0
	# getting answers from all categories
	for i in range(9, 33):
		# variables
		category_count = 0
		file_name = "temp.txt"
		curNum = 50
		# trying to get most questions, tries half amount if fails
		while curNum != 0:
			rFile = open(file_name, "w")
			file_content = urllib2.urlopen(
				"https://opentdb.com/api.php?amount={}&category={}&type=multiple&token={}"
					.format(curNum, i, token)).read()
			# checking if done
			if len(file_content) <= 17: # error on site
				rFile.close()
				break
			# checking if success
			elif file_content[17] == '0':
				# adding data to file and passing it personally syntaxed to main file
				rFile.write(file_content)
				rFile.flush()
				sortFile(wFile, file_name)
				category_count += curNum
				rFile.close()
			# checking if error
			elif file_content[17] == '3' or file_content == '2': # 3,2 = errors
				rFile.close()
				exit(1) # exiting with error
			# checking if needs less questions
			else: # 1,4 = not enough questions, else error
				curNum /= 2 # halfs the request amount

		total_count += category_count
		print "Got {} questions from category {}".format(category_count, i)
	print "Got {} Question Total!".format(total_count)

def sortFile(wFile, rFileName):
	h = HTMLParser()
	rFile = open(rFileName, 'r')
	line = rFile.readline()
	if(len(line) > 0):
		j = json.loads(line)
		for result in j['results']:
			line_details = h.unescape((u'{}||{}||{}').format(result['question'], result['correct_answer'], '||'.join(result['incorrect_answers'])))
			wFile.write(line_details.encode('utf-8', 'ignore').strip() + "\n")
		wFile.flush()
		rFile.close()


def addQuestionsToDB(main_file_name, db_name):
	main_file = open(main_file_name, 'r') # opening file with questions
	# establishing conncetion
	sql_con = sqlite3.connect(db_name)
	con_handle = sql_con.cursor()
	# adding all lines to the file
	for line in main_file:
		values = line.replace("\"", "\"\"").split('||') # getting values to array
		# checking if sufficcent values
		if(len(values) == 5): # question, correct answer, 3 wrong answers
			exec_line = ("INSERT INTO t_questions(question, correct_ans, ans2, ans3, ans4)"
							" VALUES(\"{}\", \"{}\", \"{}\", \"{}\", \"{}\")".format(
								values[0], values[1], values[2], values[3], values[4]))
			con_handle.execute(exec_line) # inserting values
	sql_con.commit() # saving changes
	sql_con.close()

def main():
	main_file_name = "Final.txt"

	#wFile = open(main_file_name, "w")
	#getFiles(wFile)
	#wFile.close()
	addQuestionsToDB(main_file_name, "trivia.db")



main()
print "Press enter to exit.."
raw_input()