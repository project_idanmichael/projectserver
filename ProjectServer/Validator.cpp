#include "Validator.h"
#define MIN_LENGTH 4
///<summary>Returns if the username is valid</summary>
///<param name="userName">the username</param>
bool Validator::isPasswordVaild(string password)
{
	bool res = false, foundDigit = false ,foundCaptial=false,foundRegular=false;
	if (password.length() >= MIN_LENGTH && password.find(' ') == -1)
	{
		for (unsigned int i = 0; i < password.length(); i++)
		{
			if (foundDigit && foundCaptial && foundRegular)
			{
				break;
			}
			else
			{
				if (isdigit(password[i]))
				{
					foundDigit = true;
				}
				else if (isupper(password[i]))
				{
					foundCaptial = true;
				}
				else if (islower(password[i]))
				{
					foundRegular = true;
				}
			}
		}
		if (foundDigit && foundCaptial && foundRegular)
		{
			res = true;
		}
	}
	
	return res;
}
bool Validator::isUsernameVaild(string userName)
{
	bool isValid = userName.length() != 0; // Checking if the username isn't empty
	if (isValid)
	{
		// Checking if first letter is a letter
		isValid = isalpha(userName[0]) != 0;
	}
	if (isValid)
	{
		for (unsigned int i = 0; isValid && i < userName.length(); i++)
		{
			isValid = userName[i] != ' ';
		}
	}
	return isValid;
}