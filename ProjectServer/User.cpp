#include "User.h"

///<summary>builds the user's name and socket</summary>
User::User(string username, SOCKET userSoc)
{
	this->_username = username;
	this->_sock = userSoc;
	this->_currRoom = nullptr;
}

User::~User(){}

Game* User::getGame()
{
	return this->_currGame;
}

Room* User::getRoom()
{
	return this->_currRoom;
}

SOCKET User::getSocket()
{
	return this->_sock;
}

string User::getUserName()
{
	return this->_username;
}

///<summary>sends the requested message to the user</summary>
void User::send(string msg)
{
	Helper::sendData(this->_sock, msg);
}

///<summary>removes the user from the room if is in any</summary>
void User::leaveRoom()
{
	if (this->_currRoom != nullptr) // checking if user is in room
	{
		this->_currRoom->leaveRoom(this);
		this->_currRoom = nullptr;
	}
}

////<summary>closes the room the user is in if the user is the creator, returns the room id or -1 if failed</summary>
int User::closeRoom()
{
	int returnCode = FAIL_CODE;
	if (this->_currRoom != nullptr) // checking if user is in room
	{
		returnCode = this->_currRoom->closeRoom(this);
		// deattaching the user from the room if room was closed successfuly
		if (returnCode != FAIL_CODE)
		{
			delete this->_currRoom;
			this->_currRoom = nullptr; //////////////////////////////////////////////////////////
		}
	}
	
	return returnCode;
}

///<summary>removes the user from the game, returns if succeeded</summary>
bool User::leaveGame()
{
	bool returnVal = false;
	this->_currRoom = nullptr; // removing user from room as well
	// Checking if user is in game
	if (this->_currGame != nullptr)
	{
		returnVal = this->_currGame->leaveGame(this); // trying to leave game
	}
	return returnVal;
}
void User::setGame(Game* game)/**game setter**/
{
	this->_currGame = game;
}
/**creating room**/
bool User::createRoom(int id, string name, int maxUsers, int quesNo, int quesTime)
{
	bool isSucc = false;
	Room* room = nullptr;
	string finalResult = "114"; // P_SEND_CREATE_ROOM
	std::vector<User*>* usersInRoom = nullptr;

	if (!this->_currRoom)//in case the user isn't in a room
	{
		this->_currRoom = new Room(id, this, name, maxUsers, quesTime, quesNo);//creating a room
		this->_currRoom->joinRoom(this);
		
		finalResult += CODE_SUCCESS;
		isSucc = true;
	}
	else
	{
		finalResult += CODE_CREATE_FAIL;
	}
	Helper::sendData(this->_sock, finalResult);
	return isSucc;
}
bool User::joinRoom(Room* room)
{
	bool isSucc = false;
	if (!this->_currRoom)//in case the user has'nt a room
	{
		room->joinRoom(this);//inserting the user to the room
		this->_currRoom = room;
		isSucc = true;
	}
	return isSucc;
}
/** deleting the room**/
void User::clearRoom()
{
	this->_currRoom = nullptr;
}