#include "Crypto.h"

string Crypto::encryptText(string text, byte key[KEY_IV_SIZE], byte iv[KEY_IV_SIZE])
{
	string finalText = text;

	finalText = Crypto::encryptBase64(finalText);
	finalText = Crypto::encryptAES(finalText, key, iv);
	finalText = Crypto::encryptBase64(finalText);

	return finalText;
}

string Crypto::decryptText(string text, byte key[KEY_IV_SIZE], byte iv[KEY_IV_SIZE])
{
	string finalText = text;
	finalText = Crypto::decryptBase64(finalText);
	finalText = Crypto::decryptAES(finalText, key, iv);
	finalText = Crypto::decryptBase64(finalText);

	return finalText;
}

string Crypto::encryptAES(string text, byte key[KEY_IV_SIZE], byte iv[KEY_IV_SIZE])
{
	string cipherText;

	CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv);

	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(cipherText));
	stfEncryptor.Put(reinterpret_cast<const unsigned char*>(text.c_str()), text.length() + 1);
	stfEncryptor.MessageEnd();

	return cipherText;
}

string Crypto::decryptAES(string text, byte key[KEY_IV_SIZE], byte iv[KEY_IV_SIZE])
{
	string decryptedText = "";
	CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);

	CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedText));
	stfDecryptor.Put(reinterpret_cast<const unsigned char*>(text.c_str()), text.size());
	stfDecryptor.MessageEnd();

	return decryptedText;
}

string Crypto::encryptBase64(string text)
{
	// Variables
	CryptoPP::Base64Encoder encoder;
	unsigned int textSize = text.size();
	string finalText;
	// moving the text to byte array
	unsigned char* textBytes = new unsigned char[textSize + 1];
	strncpy((char*)text.c_str(), (char*)textBytes, textSize);
	textBytes[textSize] = 0;

	encoder.Put(textBytes, textSize);
	encoder.MessageEnd();

	CryptoPP::word64 size = encoder.MaxRetrievable();
	if (size)
	{
		finalText.resize((unsigned int)size);
		encoder.Get((byte*)finalText.data(), finalText.size());
	}

	delete textBytes; // removing allocated memory
	return finalText;
}

string Crypto::decryptBase64(string text)
{
	string decodedString;

	CryptoPP::Base64Decoder decoder;

	decoder.Put((byte*)text.data(), text.size());
	decoder.MessageEnd();

	CryptoPP::word64 size = decoder.MaxRetrievable();
	if (size && size <= SIZE_MAX)
	{
		decodedString.resize((unsigned int)size);
		decoder.Get((byte*)decodedString.data(), decodedString.size());
	}

	return decodedString;
}