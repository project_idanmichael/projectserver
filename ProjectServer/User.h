#pragma once
#include <string>
#include <iostream>
#include <vector>
#include <WinSock2.h>
#include "Game.h"
#include "Protocol.h"
#include "Room.h"
#include "Helper.h"

#define FAIL_CODE -1

using namespace std;
class Room;
class Game;

class User
{
	public:
		User(string, SOCKET);
		~User();
		void send(string); 
		string getUserName();
		SOCKET getSocket();
		Room* getRoom();
		Game* getGame();
		void setGame(Game*);
		void clearRoom(); 
		bool createRoom(int, string, int, int, int); 
		bool joinRoom(Room*); 
		void leaveRoom(); 
		int closeRoom(); 
		bool leaveGame(); //

	private:
		string _username;
		Room* _currRoom;
		Game* _currGame;
		SOCKET _sock;


};
