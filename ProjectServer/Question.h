#pragma once
#include <string>
#include <iostream>
#define AMOUNT_ANSWERS 4
using namespace std;
class Question
{
	public:
		Question(int, string, string, string, string, string);
		~Question();
		string getQuestion();
		string* getAnswers();
		int getCorrectAnswerIndex();
		int getID();
	private:
		string _question;
		string _answers[AMOUNT_ANSWERS];
		int _correctAnswerIndex;
		int _ID;


};
