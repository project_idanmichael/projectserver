#pragma once
#include <string>
#include <iostream>
#include <vector>
#include <map>
#include "DataBase.h"
#include "User.h"

using namespace std;
class User;
class DataBase;
class Question;

#define WRONG_ANSWER_NUMBER 5

class Game
{
	public:
		Game(const vector<User*>&, int, DataBase&);
		~Game();
		void sendFirstQuestion();
		void handleFinishGame();
		bool handleNextTurn(); 
		bool handleAnswerFromUser(User*, int, int); 
		bool leaveGame(User*);
		int getID();
	private:
		vector<Question> _questions;
		vector<User*> _players;
		int _questions_no;
		int _currQuestionIndex;
		DataBase& _db; // db
		
		int _id;
		map<string, int> _results;
		int _currentTurnAnswers;
		bool InsertGameToDB();
		void initQuestionsFromDB();
		void sendQuestionToAllUsers();


};
