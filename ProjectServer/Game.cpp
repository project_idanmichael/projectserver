#include "Game.h"
#define SIZE_LEN 3

/* 'tors */

Game::Game(const vector<User*>& players, int questionNo, DataBase& db) : _db(db)
{
	this->_players = players;
	this->_questions_no = questionNo;
	this->_currQuestionIndex = 0;
	this->initQuestionsFromDB();
	// initializing the scores of the players
	for (unsigned int i = 0; i < this->_players.size(); i++)
	{
		pair<string, int> p(this->_players[i]->getUserName(), 0);
		this->_results.insert(p);
		// setting game as user's cur game
		this->_players[i]->setGame(this);
	}
	// adding the game to the table and getting the id
	this->_id = this->_db.insertNewGame();
}
Game::~Game()
{
	this->_questions.clear();
	this->_players.clear();
}


// <summary> initiates the questions of the game with questions from database </summary>
void Game::initQuestionsFromDB()
{
	this->_questions = this->_db.initQuestion(this->_questions_no);
}

/// <summary> inserts the game to the local database </summary>
bool Game::InsertGameToDB()
{
	DataBase::games.push_back(this);
	return true;
}

/// <summary> handles a user's leaving the game </summary>
bool Game::leaveGame(User* currUser)
{
	// variables
	bool isSucc = false;
	vector<User*>::iterator curr;

	// iterating the players in game to erase the leaving user
	for (curr = this->_players.begin(); curr != this->_players.end(); ++curr)
	{
		if (!currUser->getUserName().compare((*curr)->getUserName()))
		{
			this->_players.erase(curr);
			break;
		}
	}
	isSucc = this->handleNextTurn();
	return isSucc;
}


// <summary> returns the game's id </summary>
int Game::getID()
{
	return this->_id;
}


/// <summary> sends the first question to all users </summary>
void Game::sendFirstQuestion()
{
	this->sendQuestionToAllUsers();
}

/// <summary> sends the current question to all users </summary>
void Game::sendQuestionToAllUsers()
{
	unsigned int i = 0;
	string msg = "118"; //P_SEND_QUESTIONS
	string ques = this->_questions[this->_currQuestionIndex].getQuestion();
	std::string* answers = this->_questions[this->_currQuestionIndex].getAnswers();
	msg += Helper::getPaddedNumber(ques.length(), SIZE_LEN);
	msg += ques;
	// adding answers to msg
	for (i = 0; i < AMOUNT_ANSWERS; i++)
	{
		msg += Helper::getPaddedNumber(answers[i].length(), SIZE_LEN);
		msg += answers[i];
	}
	this->_currentTurnAnswers = 0;
	// sending message to all users
	for (i = 0; i < this->_players.size(); i++)
	{
		try
		{
			this->_players[i]->send(msg);
		}
		catch (exception& e)
		{
			cout << e.what() << endl;
		}
	}
}



/// <summary> sends message to the user according to his answer to the question </summary>
bool Game::handleAnswerFromUser(User* user, int answerNo, int time)
{
	// Variables
	bool notOver = false;
	bool isCorrect = false;
	string* answers = this->_questions[this->_currQuestionIndex].getAnswers();
	string send = "120";

	this->_currentTurnAnswers++; // adding another player's answer to the count

	if (user->getGame()) // checking if game not over
	{
		notOver = true;
		// checking if user answered correct and in time
		if (answerNo != WRONG_ANSWER_NUMBER &&
			answers[answerNo - 1] == answers[this->_questions[this->_currQuestionIndex].getCorrectAnswerIndex()] &&
			user->getRoom()->getQuestionTime() >= time)
		{
			isCorrect = true;
			this->_results[user->getUserName()]++; // adding correct answer to the user
			send += "1";
		}
		else
		{
			send += "0";
		}
		this->_db.addAnswerToPlayer(this->getID(), user->getUserName(), this->_questions[this->_currQuestionIndex].getID(), isCorrect ? answers[answerNo] : "", isCorrect, time);
	}
	Helper::sendData(user->getSocket(), send); // sending the reply to the user
	this->handleNextTurn(); // trying to move to next turn
	return notOver;
}

/// <summary> handles procceeding to the next turn </summary>
bool Game::handleNextTurn()
{
	bool isChanged = false;
	// checking if there are users left in the game
	if (this->_players.size()) 
	{
		// checking if all have answered and can pass to next turn
		if (this->_currentTurnAnswers >= (int)this->_players.size()) // >= since maybe some left
		{
			// Checking if not at last question
			if (this->_currQuestionIndex < (int)this->_questions.size() - 1)
			{
				isChanged = true;
				this->_currQuestionIndex++;
				this->sendQuestionToAllUsers();
			}
			else // finishes the game
			{
				this->handleFinishGame();
				
			}
		}
	}
	else // finishes the game
	{
		this->handleFinishGame();
	}
	return isChanged;
}

/// <summary> sending GAME FINISHED message to all participants </summary>
void Game::handleFinishGame()
{
	// Variables
	int playerCount = this->_players.size();
	string msg = "121" + to_string(playerCount);
	string curUserName;

	// creating the message
	for (int i = 0; i < playerCount; i++)
	{
		curUserName = this->_players[i]->getUserName();
		msg += Helper::getPaddedNumber(curUserName.size(), SIZE_USER_NAME);
		msg += curUserName;
		msg += Helper::getPaddedNumber(this->_results[curUserName], SIZE_QUESTION_COUNT);
	}

	this->_db.updateGameStatus(this->getID()); ///// CHANGED /////
	// sends a game over message to all participants
	for (unsigned int i = 0; i < this->_players.size(); i++)
	{
		try // tries to send the end game to players, ignores error if player does not exist anymore
		{
			this->_players[i]->send(msg);
			this->_players[i]->leaveGame();
		}
		catch (...){}
	}
	
}


