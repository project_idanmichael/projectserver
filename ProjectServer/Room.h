#pragma once
#include <string>
#include <iostream>
#include <vector>
#include <iterator>
#include "User.h"
#include "Protocol.h"

using namespace std;

class User;

class Room
{
	public:
		Room(int, User*, string, int, int, int);
		~Room();
		bool joinRoom(User*);
		void leaveRoom(User*);
		int closeRoom(User*);
		vector<User*> getUsers();
		string getUsersListMessage();
		int getQuestionsNo();
		int getQuestionTime();
		int getID();
		string getName();
	private:
		vector<User*> _users;
		User* _admin;
		int _maxUsers;
		int _questionTime;
		int _questionNo;
		string _name;
		int _ID;
		string getUsersAsString(vector<User*>, User*);
		void sendMessage(string);
		void sendMessage(User*, string);
};
