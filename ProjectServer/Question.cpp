#include "Question.h"

#define ANSWER_NUM 4
#define WRONG_ANSWER_NUM 3
#define LAST_ANSWER_INDEX 3
#define SIZE 5

Question::Question(int id, string question, string option1, string option2, string option3, string option4)
{
	this->_ID = id;
	this->_question = question;
	this->_correctAnswerIndex = rand() % ANSWER_NUM;

	string* wrongAnswers[] = { &option2, &option3, &option4 }; // entering all wrong answers to array for higher efficency
	// entering all wrong answers to question's answers
	for (int i = 0; i < WRONG_ANSWER_NUM; i++)
	{
		// enters the answer to the given index, if index is for correct answer - places it in last index instead
		if (i != this->_correctAnswerIndex)
		{
			this->_answers[i] = *wrongAnswers[i];
		}
		else
		{
			this->_answers[LAST_ANSWER_INDEX] = *wrongAnswers[i];
		}
	}
	
	// adding the correct answer to correct index
	this->_answers[this->_correctAnswerIndex] = option1;
}

Question::~Question(){}

string Question::getQuestion()
{
	return this->_question;
}

string* Question::getAnswers()
{
	return this->_answers;
}

int Question::getCorrectAnswerIndex()
{
	return this->_correctAnswerIndex;
}
int Question::getID()
{
	return this->_ID;
}