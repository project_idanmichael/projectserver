#include "TriviaServer.h"
#include "ReceivedMessage.h"
#include "Validator.h"
#include "Protocol.h"
#include "Helper.h"
#include "Crypto.h"
#include <thread>

#define INDEX_USERNAME 0
#define INDEX_PASSWORD 1
#define INDEX_EMAIL 2
#define MESSAGE_LEN 4
#define NUM_OF_DETAILS 2
#define SIZE_VECTOR_BEST_SCORES 6

int TriviaServer::_roomIDSequence = 0;
condition_variable* condQueue;

/* 'tors */
TriviaServer::TriviaServer()
{
	condQueue = new condition_variable();
	WSADATA s = {};
	if (WSAStartup(MAKEWORD(2, 2), &s) != 0)
	{
		throw exception("WSAstartup failed");
	}
	//trying to create a socket
	this->_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (this->_socket == INVALID_SOCKET)
	{
		throw std::exception("Error: creating socket failed");
	}
}
TriviaServer::~TriviaServer()
{
	// variables
	User* curUser = nullptr;
	ReceivedMessage* curMsg = nullptr;
	// removing users and games
	for (map<SOCKET, User*>::iterator it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); it++)
	{
		curUser = it->second;
		try
		{
			closesocket(curUser->getSocket()); // closing socket
		}
		catch (...){}
		// deleting
		if (curUser->getGame())
		{
			delete curUser->getGame();
		}
		delete curUser;
	}
	// removing rooms
	for (map<int, Room*>::iterator it = this->_roomList.begin(); it != this->_roomList.end(); it++)
	{
		delete it->second;
	}
	// removing message queue
	while (!this->_queRcvMessages.empty())
	{
		curMsg = this->_queRcvMessages.front();
		this->_queRcvMessages.pop();
		delete curMsg;

	}
	// removing conditional
	delete condQueue;	
}

/**the main function of class**/
void TriviaServer::server()
{
	cout << "Initiating server" << endl;
	// trying to bind to socket
	try
	{
		this->bindAndListen();//binding and listening
	}
	catch (exception ex)
	{
		cout << "ERROR : " << ex.what() << endl;
		return;
	}
	//creating a thread for message queue
	thread tMsg(&TriviaServer::handleReceivedMessages,this);
	tMsg.detach();
	while (this->_socket != SOCKET_ERROR)//loop for inserting users
	{
		this->accept();
	}
	
}


///<summary>creats a thread for accepted client</sumamry>
void TriviaServer::accept()
{
	SOCKET soc = ::accept(this->_socket, NULL, NULL);
	if (soc == INVALID_SOCKET)
	{
		throw std::exception("failed creating client socket");
	}
	cout << "New Connection on Socket: " << soc << endl;
	thread tClientHandler(&TriviaServer::clientHandler,this, soc);
	tClientHandler.detach(); //creating a thread for clientHandler
}

///<summary>binds to a socket</summary>
void TriviaServer::bindAndListen()
{
	struct sockaddr_in service = { 0 };

	service.sin_family = AF_INET;
	service.sin_addr.S_un.S_addr = INADDR_ANY;
	service.sin_port = htons(8820);


	if (::bind(this->_socket, (struct sockaddr*)&service, sizeof (service)) == SOCKET_ERROR)
	{
		exception ex("ERROR : Bind Error");
		throw ex;
	}
	else if (listen(this->_socket, SOMAXCONN) == SOCKET_ERROR)
	{
		exception ex("ERROR : Listen Error");
		throw ex;
	}
}

///<summary>the function removes a connected client</summary>
void TriviaServer::safeDeleteUser(ReceivedMessage* msg)
{
	try//trying to close the user's socket
	{
		SOCKET sk = msg->getSock();
		this->handleSignout(msg);//signing out the user
		
		closesocket(sk);//closing socket
		cout << "Removed User in Socket: " << sk << endl;
	}
	catch (...)
	{
		cout << "Error: removing user failed" << endl;
	}
}




///<summary>adds the recieved message to the message queue</summary>
void TriviaServer::addReceivedMessage(ReceivedMessage* msg)
{
	this->_mtxReceivedMessages.lock();
	this->_queRcvMessages.push(msg);
	this->_mtxReceivedMessages.unlock();
	condQueue->notify_all();

}

///<summary>builds the message according to the message code received and returns it</summary>
ReceivedMessage* TriviaServer::buildReceivedMessage(SOCKET clientSocket, int msgCode, byte* key, byte* iv)
{
	// Variables
	int code = msgCode;
	ReceivedMessage* msg = new ReceivedMessage(clientSocket, msgCode, key, iv);
	vector<string>& values = msg->getValues();
	int curLen = 0, count = 0;

	msg->setUser(this->getUserBySocket(clientSocket)); // sets the message's user

	// checking all protocols to build message accordingly
	switch (code)
	{
	case P_SIGN_IN:
		for (int i = 0; i < 2; i++) // two parameters - username and password
		{
			// getting text length and adding it
			curLen = Helper::getIntPartFromSocket(clientSocket, SIZE_DETAIL);
			values.push_back(Helper::getStringPartFromSocket(clientSocket, curLen));
		}
		break;
	case P_SIGN_UP:
		for (int i = 0; i < 3; i++) // three parameters - username, password and email
		{
			// getting text length and adding it
			curLen = Helper::getIntPartFromSocket(clientSocket, SIZE_DETAIL);
			values.push_back(Helper::getStringPartFromSocket(clientSocket, curLen));
		}
		break;
	case P_REQ_JOIN_ROOM: // codes with only 4 bytes of 1 variable
		values.push_back(Helper::getStringPartFromSocket(clientSocket, SIZE_ROOM_ID));
		break;
	case P_REQ_CREATE_ROOM:
		curLen = Helper::getIntPartFromSocket(clientSocket, SIZE_DETAIL);
		values.push_back(Helper::getStringPartFromSocket(clientSocket, curLen));
		values.push_back(Helper::getStringPartFromSocket(clientSocket, 1)); // getting player count
		values.push_back(Helper::getStringPartFromSocket(clientSocket, SIZE_QUESTION_COUNT));
		values.push_back(Helper::getStringPartFromSocket(clientSocket, SIZE_QUESTION_TIME));
		break;
	case P_REQ_CLIENT_ANSWER:
		values.push_back(Helper::getStringPartFromSocket(clientSocket, SIZE_ANSWER_NUMBER));
		values.push_back(Helper::getStringPartFromSocket(clientSocket, SIZE_QUESTION_TIME));
		break;
	case P_REQ_ROOM_USER_LIST:
		values.push_back(Helper::getStringPartFromSocket(clientSocket, SIZE_ROOM_ID));
		break;
	}
	return msg;
}

///<summary>handles the received message queue</summary>
void TriviaServer::handleReceivedMessages()
{
	int toCheck;
	ReceivedMessage* msg = nullptr;
	while (true)
	{
		unique_lock<mutex> locker(this->_mtxReceivedMessages);
		try
		{
			condQueue->wait(locker);//waiting for opening queue
		}
		catch (...)
		{
			this->safeDeleteUser(msg);
			break;
		}
		if (!this->_queRcvMessages.empty())//in case the queue is not empty
		{
			msg = this->_queRcvMessages.front();
			this->_queRcvMessages.pop();
			//saving message and removing it from queue
		}
		
		locker.unlock();
		msg->setUser(this->getUserBySocket(msg->getSock()));//setting the message user to the user from socket
		try
		{
			toCheck = msg->getMessageCode();
			/*if (toCheck == P_EXIT)
			{
				this->safeDeleteUser(msg);
			}*/
		}
		catch (exception& e)
		{
			cout << e.what() << endl;
		}
		bool isSucc = true;
		try//calling the matching handle
		{
			if (toCheck == P_SIGN_IN)
			{
				User* toIns = this->handleSignin(msg);
				if (toIns)
				{
					pair<SOCKET, User*> p(toIns->getSocket(), toIns);
					this->_connectedUsers.insert(p);
				}
				else
				{
					cout << "Error signing in." << endl;
				}
			}
			else if (toCheck == P_SIGN_UP)
			{
				if (!this->handleSignup(msg))
				{
					isSucc = false;
				}
			}
			else if (toCheck == P_SIGN_OUT)
			{
				this->handleSignout(msg);
			}
			else if (toCheck == P_REQ_CREATE_ROOM)
			{
				if (!this->handleCreateRoom(msg))
				{
					isSucc = false;
				}
				else
				{
					pair<int, Room*> p(msg->getUser()->getRoom()->getID(), msg->getUser()->getRoom());
					this->_roomList.insert(p);
				}
			}
			else if (toCheck == P_REQ_JOIN_ROOM)
			{
				if (!this->handleJoinRoom(msg))
				{
					isSucc = false;
				}
			}
			else if (toCheck == P_REQ_LEAVE_ROOM)
			{
				if (!this->handleLeaveRoom(msg))
				{
					isSucc = false;
				}
			}
			else if (toCheck == P_REQ_CLOSE_ROOM)
			{
				int id = msg->getUser()->getRoom()->getID();
				Room* room = this->_roomList[id];
				if (!this->handleCloseRoom(msg))
				{
					isSucc = false;
				}
				else
				{
					//this->_roomList.erase(it);
				}
			}
			else if (toCheck == P_REQ_ROOM_LIST)
			{
				this->handleGetRooms(msg);
			}
			else if (toCheck == P_REQ_ROOM_USER_LIST)
			{
				this->handleGetUsersInRoom(msg);
			}
			else if (toCheck == P_START_GAME)
			{
				this->handleStartGame(msg);
			}
			else if (toCheck == P_LEAVE_GAME)
			{
				this->handleLeaveGame(msg);
			}
			else if (toCheck == P_REQ_CLIENT_ANSWER)
			{
				this->handlePlayerAnswer(msg);
			}
			else if (toCheck == P_REQ_BEST_SCOERS)
			{
				this->handleGetBestScores(msg);
			}
			else if (toCheck == P_REQ_PERSONAL_STATUS)
			{
				this->handleGetPersonalStatus(msg);
			}
			else
			{
				this->safeDeleteUser(msg);
			}
		}
		catch (...)
		{
			this->safeDeleteUser(msg);
		}
		delete msg;
	}
}

///<summary>handles client communication and adds messages to message queue</summary>
void TriviaServer::clientHandler(SOCKET clientSocket)
{
	int msgCode = 0;
	ReceivedMessage* msg;
	byte key[SIZE_KEY_IV_CRYPT] = { 0 }, iv[SIZE_KEY_IV_CRYPT] = { 0 };

	try // trying to get message, ends communcation if failed
	{
		msgCode = Helper::getMessageTypeCode(clientSocket);
	}
	catch (...)
	{
		msgCode = P_EXIT;
	}
	// getting the encryption details
	if (msgCode == P_INIT_CRYPT)
	{
		this->handleGetKeyAndIV(clientSocket, &key, &iv);
	}
	else
	{
		msgCode = P_EXIT;
	}

	while (msgCode != 0 && msgCode != P_EXIT) // checking message code is valid to add
	{
		// trying to get message, ends communication if failed
		try
		{
			msgCode = Helper::getMessageTypeCode(clientSocket);
		}
		catch (...)
		{
			break;
		}
		// trying to build the rest of the message, ends communication if failed
		try
		{
			msg = this->buildReceivedMessage(clientSocket, msgCode, key, iv);
		}
		catch (...)
		{
			break;
		}
		// adding message to the message queue with mutex protection
		if (msg != nullptr) // checking message was created
		{
			this->addReceivedMessage(msg);
		}
	}

	// adding "end comminucation" message to queue
	msg = this->buildReceivedMessage(clientSocket, P_EXIT, nullptr, nullptr);
	this->addReceivedMessage(msg);
}



///<summary>Checks if the given user exists in the given map</summary>
///<param name="users">the map of the users to search in</param>
///<param name="user">the user to search</param>
bool TriviaServer::isUserExist(map<string, string>& users, string user)
{
	// Variables
	map<string, string>::iterator it = users.begin();
	bool userFound = false;

	// iterating the user map to check if the user exists

	for (it; it != users.end(); it++)
	{
		if (it->first == user)
		{
			userFound = true;
			break;
		}
	}
	return userFound;
}

///<summary>returns a user by a name if the user is connected</summary>
User* TriviaServer::getUserByName(string name)//////
{
	map<SOCKET, User*>::iterator curr;
	User* ret = nullptr;
	for (curr = this->_connectedUsers.begin(); curr != this->_connectedUsers.end(); ++curr)//the loop runs all over the connected users
	{
		if (!curr->second->getUserName().compare(name))//checks if there is a username with the gotten username
		{
			ret = curr->second;
			break;
		}
	}
	return ret;
}/////

///<summary>returns a user by a name if the user is connected</summary>
User* TriviaServer::getUserBySocket(SOCKET userSoc)//////
{
	map<SOCKET, User*>::iterator curr;
	User* ret = nullptr;
	for (curr = this->_connectedUsers.begin(); curr != this->_connectedUsers.end(); ++curr)//the loop runs all over the connected users
	{
		if (curr->first == userSoc)//checks if there is a username with the gotten username
		{
			ret = curr->second;
			break;
		}
	}
	return ret;
}

///<summary>returns a room by a known id</summary>
Room* TriviaServer::getRoomByID(int id)
{
	Room* res = nullptr;
	map<int, Room*>::iterator curr;
	for (curr = this->_roomList.begin(); curr != this->_roomList.end(); ++curr)//loop for searching the room with the correct id
	{
		if (curr->first == id)
		{
			res = curr->second;
			break;
		}
	}
	return res;
}




///<summary>Signs up a user from the recieved message details</summary>
bool TriviaServer::handleSignup(ReceivedMessage* msg)
{
	// Variables
	bool returnVal = false;
	vector<string>& info = msg->getValues();
	string finalCode = "104"; // sign up reply
	char code = CODE_SUCCESS;
	string password = "";

	// decrypting password
	password = Crypto::decryptText(info[INDEX_PASSWORD], msg->getKey(), msg->getIV());

	// Checking if user passes all conditions
	if (!Validator::isPasswordVaild(info[INDEX_PASSWORD])) // password
	{
		code = P_SIGN_UP_BAD_PASS;
	}
	else if (!Validator::isUsernameVaild(info[INDEX_USERNAME])) // username
	{
		code = P_SIGN_UP_BAD_USER;
	}
	else if (this->_db.isUserExists(info[INDEX_USERNAME])) // username exist
	{
		code = P_SIGN_UP_USER_EXIST;
	}
	// Adding the user if passes all conditions
	else
	{
		//DataBase::addNewUser
		this->_db.addNewUser(info[INDEX_USERNAME], password, info[INDEX_EMAIL]);   //insert(std::pair<string, string>(info[INDEX_USERNAME], info[INDEX_PASSWORD]));
		returnVal = true;
	}
	finalCode += code;
	Helper::sendData(msg->getSock(), finalCode); // sends the final message to the user
	return returnVal;
}

///<summary>signs in a user</sumamry>
User* TriviaServer::handleSignin(ReceivedMessage* msg)
{
	map<string, string>::iterator curr;
	User* ret = nullptr;
	bool isMatch = false;
	string finalResult = "102"; // P_SIGN_IN_REPLY
	string password = "";

	// decrypting password
	password = Crypto::decryptText(msg->getValues()[INDEX_PASSWORD], msg->getKey(), msg->getIV());


	if (this->_db.isUserAndPassMatch(msg->getValues()[INDEX_USERNAME], password))
	{
		//in case of true at the previous condition,checks if the user is already connected
		if (!this->getUserByName(msg->getValues()[INDEX_USERNAME]))
		{
			finalResult += CODE_SUCCESS;
			ret = new User(msg->getValues()[0], msg->getSock());//creating a new user
			isMatch = true;
			cout << "User " << msg->getValues()[0] << " signed in" << endl;
		}
		else
		{
			finalResult += CODE_SIGN_IN_CONNECTED;
		}
	}
	else// Checking if one of the codes was already enetered
	{
		finalResult += CODE_SIGN_IN_WRONG;
	}
	Helper::sendData(msg->getSock(), finalResult);
	return ret;
}

///<summary>signs out a user and removes him from the room, closes the room if created by him</summary>
void TriviaServer::handleSignout(ReceivedMessage* msg)
{
	// Variables
	map<SOCKET, User*>::iterator it = this->_connectedUsers.begin();
	bool userFound = false;
	User* userDel = nullptr; // the user to delete

	// Iterating the connected users to check if the user is connected
	for (it; it != this->_connectedUsers.end(); it++)
	{
		// Checking if cuurent user is searched user
		if (it->second->getUserName() == msg->getUser()->getUserName()) ///////////////////////////////////////////////////////// mabye not strings
		{
			userDel = it->second;
			this->_connectedUsers.erase(it); // deletes the user
			userFound = true;
			break;
		}
	}

	// removing user from rooms and game, closes the room if he owns it
	if (userFound)
	{
		if (!this->handleCloseRoom(msg))
		{
			this->handleLeaveRoom(msg);
		}
		this->handleLeaveGame(msg);
	}
	delete userDel; // removes the user from memory
}

///<summary>sends the user who sent the message the room list</summary>
void TriviaServer::handleGetRooms(ReceivedMessage* msg)
{
	string sendMsg = "106";
	map<int, Room*>::iterator it = this->_roomList.begin();
	sendMsg += Helper::getPaddedNumber(this->_roomList.size(), SIZE_ROOM_COUNT);

	for (it; it != this->_roomList.end(); it++)
	{
		sendMsg += Helper::getPaddedNumber(it->first, SIZE_ROOM_ID);
		sendMsg += Helper::getPaddedNumber(it->second->getName().size(), SIZE_ROOM_NAME);
		sendMsg += it->second->getName();
	}
	Helper::sendData(msg->getSock(), sendMsg);// sending the message to client
}

///<summary>handler for creating a room</summary>
bool TriviaServer::handleCreateRoom(ReceivedMessage* msg)//////
{
	bool isCreated = false;
	if (msg->getUser())//checking if the message has a sender
	{
		TriviaServer::_roomIDSequence++;//making an id for the room
		if (msg->getUser()->createRoom(TriviaServer::_roomIDSequence, msg->getValues()[0], stoi(msg->getValues()[1]), stoi(msg->getValues()[2]), stoi(msg->getValues()[3])))//in case of room created
		{
			pair<int, Room*> p(TriviaServer::_roomIDSequence, msg->getUser()->getRoom());//making a pair with the new room and id
			this->_roomList.insert(p);//inserting the pair to the room's list
			isCreated = true;
		}
	}
	return isCreated;
}/////

///<summary>handles the closing of the room with the user in the message, returns if the room was closed</summary>
bool TriviaServer::handleCloseRoom(ReceivedMessage* msg)
{
	map<int, Room*>::iterator it;
	int roomId = 0;
	if (msg->getUser() != nullptr) // checking if user exists
	{
		roomId = msg->getUser()->closeRoom(); // checking if room closed
	}
	// removing room from room map
	if (roomId != -1)
	{
		for (it = this->_roomList.begin(); it != this->_roomList.end(); it++)
		{
			if (it->second->getID() == roomId)
			{
				this->_roomList.erase(it);
				break;
			}
		}
	}
	return roomId != -1;
}

///<summary>message joining room handler</summary>
bool TriviaServer::handleJoinRoom(ReceivedMessage* msg)/////
{
	bool isJoined = false;
	Room* joiner = nullptr;
	string finalCode = "110";  //P_SEND_JOIN_ROOM
	if (msg->getUser())//checking if the message has a sender
	{
		joiner = this->getRoomByID(stoi(msg->getValues()[0]));//finding the room with the id from message*
		if (joiner)//in case the room is exist
		{
			finalCode += CODE_SUCCESS;
			finalCode += Helper::getPaddedNumber(joiner->getQuestionsNo(), 2);
			finalCode += Helper::getPaddedNumber(joiner->getQuestionTime(), 2);
			msg->getUser()->joinRoom(joiner);//calling joinRoom
			isJoined = true;
		}
		else
		{
			finalCode += CODE_ROOM_OTHER;
		}
		Helper::sendData(msg->getSock(), finalCode); // sending message to client
		// sending new list to all if connected successfuly
		if (isJoined && joiner)
		{
			// Sending list to all users
			finalCode = joiner->getUsersListMessage(); // getting the message
			vector<User*>& users = joiner->getUsers();
			for (unsigned int i = 0; i < users.size(); i++)
			{
				Helper::sendData(users[i]->getSocket(), finalCode);
			}
		}
	}
	return isJoined;
}/////

///<summary>removes the user from the room if is in any, returns if removed</summary>
bool TriviaServer::handleLeaveRoom(ReceivedMessage* msg)
{
	bool returnVal = false;
	string finalCode;
	Room* curRoom = nullptr;
	if (msg->getUser() != nullptr && msg->getUser()->getRoom() != nullptr)
	{
		curRoom = msg->getUser()->getRoom(); // saving the room for sending update
		msg->getUser()->leaveRoom();
		// Sending updated user list to all users
		finalCode = curRoom->getUsersListMessage(); // getting the message
		vector<User*>& users = curRoom->getUsers();
		// sending to each 
		for (unsigned int i = 0; i < users.size(); i++)
		{
			Helper::sendData(users[i]->getSocket(), finalCode);
		}
		returnVal = true;
	}
	return returnVal;
}

///<summary>sends the user who sent the message the user list in the room he is in</summary>
void TriviaServer::handleGetUsersInRoom(ReceivedMessage* msg)
{
	// sends the message to the user
	Helper::sendData(msg->getUser()->getSocket(), this->getRoomByID(stoi(msg->getValues()[0]))->getUsersListMessage());
}

/// <summary>handles the start of the game, sends first question if created </summary>
void TriviaServer::handleStartGame(ReceivedMessage* msg)
{
	// Variables
	Room* currRoom = msg->getUser()->getRoom();
	string res = "118";
	Game* game = nullptr;
	bool isCreated = false;

	// trying to create the game, will fail if database will not open
	try
	{
		game = new Game(currRoom->getUsers(), currRoom->getQuestionsNo(), this->_db);
		isCreated = true;
	}
	catch (...)
	{
		cout << "ERROR : Failed creating game" << endl;
		res += "0";
		Helper::sendData(msg->getSock(), res);
	}

	if (isCreated)
	{
		// finding the room and removing it from the room list
		for (map<int, Room*>::iterator curr = this->_roomList.begin(); curr != this->_roomList.end(); ++curr)
		{
			// comparing current room with wanted room through ID
			if (curr->second->getID() == currRoom->getID())
			{
				// removing the room from the list
				this->_roomList.erase(curr);
				break;
			}
		}
		// sends the first question to all participants
		game->sendFirstQuestion();
	}
}

/// <summary>handles the answer received by the player</summary>
void TriviaServer::handlePlayerAnswer(ReceivedMessage* msg)
{
	Game* game = msg->getUser()->getGame();
	if (game)
	{
		// passes the handle to the game, if return false then game is over and should be deleted from memory
		if (!game->handleAnswerFromUser(msg->getUser(), stoi(msg->getValues()[0]), stoi(msg->getValues()[1])))
		{
			delete game;
		}
	}
}

/// <summary>handles a user leaving the game</summary>
void TriviaServer::handleLeaveGame(ReceivedMessage* msg)
{
	User* user = msg->getUser();
	// trying to remove user from game, removes game from memory if succeeded
	if (user && user->leaveGame())
	{
		Game* game = msg->getUser()->getGame();
		delete game;
	}
}

/// <summary>gets the best users' scores from the database and sends back to the user</summary>
void TriviaServer::handleGetBestScores(ReceivedMessage* msg)
{
	vector<string> scores = this->_db.getBestScores();
	string msgToSend = "124";
	int i = 0;

	for (unsigned int i = 0; i < SIZE_VECTOR_BEST_SCORES; i = i + 2)
	{
		if (!scores.empty() && scores.size() > i)
		{
			msgToSend += Helper::getPaddedNumber(scores[i].length(), SIZE_USER_NAME);
			msgToSend += scores[i];
			msgToSend += Helper::getPaddedNumber(stoi(scores[i + 1]), SIZE_CORRECT_ANSWERS);
		}
		else
		{
			msgToSend += STRING_NO_DATA;
		}
	}
	Helper::sendData(msg->getSock(), msgToSend);
}

/// <summary>gets the requesting user's personal status and sends it back to him<summary>
void TriviaServer::handleGetPersonalStatus(ReceivedMessage* msg)
{
	vector<string> status = this->_db.getPersonalStatus(msg->getUser()->getUserName());
	string msgToSend = "126";
	// checking if status is empty
	if (status.empty())
	{
		msgToSend += "0000"; // empty info
	}
	else
	{
		msgToSend += Helper::getPaddedNumber(stoi(status[INDEX_GAME_COUNT]), SIZE_PERSONAL_GAMES);
		msgToSend += Helper::getPaddedNumber(stoi(status[INDEX_CORRECT_ANS_COUNT]), SIZE_CORRECT_ANSWERS);
		msgToSend += Helper::getPaddedNumber(stoi(status[INDEX_WRONG_ANS_COUNT]), SIZE_WRONG_ANSWERS);
		msgToSend += Helper::getPaddedNumber(stoi(status[INDEX_AVERAGE_TIME_ANS]) * 100, SIZE_AVG_ANSWER_TIME);
	}
	Helper::sendData(msg->getSock(), msgToSend);
}

/// <summary>handles the set up of the key and iv for the user</summary>
void TriviaServer::handleGetKeyAndIV(SOCKET clientSocket, byte(*key) [SIZE_KEY_IV_CRYPT], byte(*iv)[SIZE_KEY_IV_CRYPT])
{
	// gets the key and iv from client
	byte* setKey = (byte*)Helper::getPartFromSocket(clientSocket, SIZE_KEY_IV_CRYPT, 0);
	byte* setIV = (byte*)Helper::getPartFromSocket(clientSocket, SIZE_KEY_IV_CRYPT, 0);
	// changes the values of the user's key and iv
	for (int i = 0; i < SIZE_KEY_IV_CRYPT; i++)
	{
		(*key)[i] = setKey[i];
		(*iv)[i] = setIV[i];
	}
}