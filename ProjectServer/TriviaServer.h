#pragma once
#include <string>
#include <iostream>
#include <vector>
#include <WinSock2.h>
#include <Windows.h>
#include <map>
#include <mutex>
#include <thread>
#include <queue>
#include "User.h"
#include "DataBase.h"
#include "Room.h"
#include "ReceivedMessage.h"
#include <condition_variable>

#define STRING_NO_DATA "00000000"

using namespace std;

class TriviaServer
{
	public:
		TriviaServer(); 
		~TriviaServer(); 
		void server(); 
	private:
		SOCKET _socket;
		map<SOCKET, User*> _connectedUsers;
		DataBase _db;
		map<int, Room*> _roomList;
		mutex _mtxReceivedMessages;
		queue<ReceivedMessage*> _queRcvMessages;
		static int _roomIDSequence;

		bool isUserExist(map<string, string>& users, string user);
		void bindAndListen(); //
		void accept(); //
		void clientHandler(SOCKET); //
		void safeDeleteUser(ReceivedMessage*); //
		User* handleSignin(ReceivedMessage*); 
		bool handleSignup(ReceivedMessage*); 
		void handleSignout(ReceivedMessage*); 
		void handleLeaveGame(ReceivedMessage*); 
		void handleStartGame(ReceivedMessage*); 
		void handlePlayerAnswer(ReceivedMessage*);

		bool handleCreateRoom(ReceivedMessage*);
		bool handleCloseRoom(ReceivedMessage*);
		bool handleJoinRoom(ReceivedMessage*); 
		bool handleLeaveRoom(ReceivedMessage*); 
		void handleGetUsersInRoom(ReceivedMessage*); 
		void handleGetRooms(ReceivedMessage*); 

		void handleGetBestScores(ReceivedMessage*);
		void handleGetPersonalStatus(ReceivedMessage*);
		void handleReceivedMessages(); //
		void addReceivedMessage(ReceivedMessage*); //
		ReceivedMessage* buildReceivedMessage(SOCKET,int,byte*,byte*);
		User* getUserByName(string);
		User* getUserBySocket(SOCKET);
		Room* getRoomByID(int);

		void handleGetKeyAndIV(SOCKET clientSocket, byte (*)[SIZE_KEY_IV_CRYPT], byte (*)[SIZE_KEY_IV_CRYPT]);




};

