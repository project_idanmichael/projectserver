#pragma once

#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <cstdio>
#include <iostream>
#include "osrng.h"
#include "modes.h"
#include <string.h>
#include <cstdlib>
#include "des.h"
#include "md5.h"
#include "hex.h"
#include "base64.h"

#define KEY_IV_SIZE 16

using namespace std;


class Crypto
{
public:
	static string encryptText(string, byte[KEY_IV_SIZE], byte[KEY_IV_SIZE]);
	static string decryptText(string, byte[KEY_IV_SIZE], byte[KEY_IV_SIZE]);
private:
	static string encryptAES(string, byte[], byte[]);
	static string decryptAES(string, byte[KEY_IV_SIZE], byte[KEY_IV_SIZE]);
	static string encryptBase64(string);
	static string decryptBase64(string);
	Crypto(){} // not allowing instance of class
};
