#include "DataBase.h"
#include "TriviaServer.h"

vector<Game*> DataBase::games; // initializing static database


/* 'tors */
DataBase::DataBase()
{
	if (sqlite3_open(FILE_NAME, &this->_db))
	{
		exception ex("ERROR : Database failed to open");
		throw ex;
	}
}
DataBase::~DataBase()
{
	sqlite3_close(this->_db);
}



/// <summary> returns {questionNo} random questions from the database </summary>
vector<Question> DataBase::initQuestion(int questionNo)
{
	// order by RAND() LIMIT
	char* errMsg = nullptr;
	vector<Question> result;
	string sql = "SELECT * FROM t_questions order by RANDOM() LIMIT " + to_string(questionNo) + ";";
	if (sqlite3_exec(this->_db, sql.c_str(), DataBase::callbackQuestions, &result, &errMsg))
	{
		cout << "DB ERROR : " << errMsg << endl;
	}
	return result;
}



///<summary>checks if the given user exists in the database</summary>
bool DataBase::isUserExists(string user)
{
	// Variables
	bool isFound = false;
	char* errMsg = nullptr;
	string sql = "SELECT * FROM t_users WHERE username = \"" + user + "\";";

	if (sqlite3_exec(this->_db, sql.c_str(), DataBase::callbackIsCalled, &isFound, &errMsg)) // calls the query to find the user
	{
		cout << "DB ERROR : " << errMsg << endl;
	}
	return isFound;
}

///<sumamry>checks if user with this password exists in database</summary>
bool DataBase::isUserAndPassMatch(string username, string password)
{
	// variables
	bool returnVal = false;
	char* errmsg = nullptr;
	string sql = "SELECT * FROM t_users WHERE username = \"" + username + "\" AND password = \"" + password + "\";";

	// calling function on database, checks if called successfuly
	if (sqlite3_exec(this->_db, sql.c_str(), DataBase::callbackIsCalled, &returnVal, &errmsg))
	{
		cout << "DB ERROR : " << errmsg << endl;
	}

	return returnVal;
}



/// <summary>adds a new user to the database</sumamry>
bool DataBase::addNewUser(string username, string password, string email)
{
	// variables
	char* errmsg = nullptr;
	bool returnVal;
	// creating the command
	string sql = "INSERT INTO t_users(username, password, email) VALUES(\"";
	sql += username + "\",\"" + password + "\",\"" + email + "\");";

	// executing command
	returnVal = !sqlite3_exec(this->_db, sql.c_str(), nullptr, nullptr, &errmsg);
	// if failed prints error
	if (!returnVal && errmsg)
	{
		cout << "DB ERROR : " << errmsg << endl;
	}

	return returnVal;
}

/// <summary> adds the given answer to the requested player in the database, returns if succeeded </summary>
bool DataBase::addAnswerToPlayer(int id, string username, int questionID, string playerAns, bool isCorrect, int ansTime)
{
	bool isInserted = false;
	char* errMsg = nullptr;
	string sql = "INSERT into t_players_answers (game_id,username,question_id,player_answer,is_correct,answer_time) VALUES (" + to_string(id) + ",'" + username + "'," + to_string(questionID) + ",'" + playerAns + "'," + to_string(isCorrect) + "," + to_string(ansTime) + ");";
	if (sqlite3_exec(this->_db, sql.c_str(), NULL, NULL, &errMsg))
	{
		cout << "DB ERROR : " << errMsg << endl;
	}
	else
	{
		isInserted = true;
	}
	return isInserted;
}

/// <summary> updates the requested game's status in the database, returns if succeeded </summary>
bool DataBase::updateGameStatus(int id)
{
	// variables
	bool isUpdate = false;
	char* errMsg = nullptr;
	string sql = "UPDATE t_games SET status=1, end_time=DateTime('now') WHERE game_id=" + to_string(id) + ";";

	// trying to execute the command to the database
	if (sqlite3_exec(this->_db, sql.c_str(), NULL, NULL, &errMsg))
	{
		cout << "DB ERROR : " << errMsg << endl;
	}
	else
	{
		isUpdate = true;
	}
	return isUpdate;
}

/// <summary> inserts a new game to the database, returns its id </summary>
int DataBase::insertNewGame()
{
	// variables
	int res = 0;
	char* errMsg = nullptr;

	string sql = "INSERT INTO t_games (status, start_time,end_time) VALUES(0, DateTime('now'), NULL);";
	// trying to execute the sql on the database
	if (sqlite3_exec(this->_db, sql.c_str(), NULL, NULL, &errMsg))
	{
		cout << "DB ERROR : " << errMsg << endl;
		return -1;
	}
	// gets the id of the game just entered
	sql = "SELECT game_id FROM t_games WHERE game_id=last_insert_rowid();";
	if (sqlite3_exec(this->_db, sql.c_str(), DataBase::callbackCount, &res, &errMsg))
	{
		cout << "DB ERROR : " << errMsg << endl;
		return -1;
	}
	return res;
}



///<summary> returns a vector of all best scores, vector as: {username, result, ..}</summary>
vector<string> DataBase::getBestScores()
{
	// variables
	vector<string> scoresAndUsers;
	char* errmsg = nullptr;
	string sql = "SELECT username, COUNT(*) FROM t_players_answers WHERE is_correct=1 GROUP BY username ORDER BY COUNT(*) DESC LIMIT 3;";

	// getting the best scores from the database using the callback function
	if (sqlite3_exec(this->_db, sql.c_str(), DataBase::callbackBestScores, &scoresAndUsers, &errmsg)) // checks if done successfuly
	{
		cout << "DB ERROR : " << errmsg << endl;
	}

	return scoresAndUsers;
}

///<summary> returns the personal status of the given user, vector appears as in uml </summary>
vector<string> DataBase::getPersonalStatus(string username)
{
	// variables
	vector<string> info; // will contain all the personal info
	string sql = "SELECT COUNT(DISTINCT game_id),"; // game count
	sql += "SUM(CASE is_correct WHEN 1 then 1 else 0 end),"; // correct answer count
	sql += "SUM(CASE is_correct WHEN 1 then 0 else 1 end), "; // wrong answer count
	sql += "AVG(ANSWER_TIME) "; // average answer time
	sql += "FROM t_players_answers WHERE username = \"" + username + "\";" ;
	char* errmsg = nullptr;

	// getting the personal info from the database using the callback function
	if (sqlite3_exec(this->_db, sql.c_str(), DataBase::callbackPersonalStatus, &info, &errmsg))
	{
		cout << "DB ERROR : " << errmsg << endl;
	}

	return info;
}



/// <summary> callback for adding the questions from the database to the given question vector </summary>
int DataBase::callbackQuestions(void* output, int argc, char** argv, char** azColName)
{
	// variables
	int id = 0;
	int returnVal = SQLITE_ERROR;
	string ques;
	string correct;
	string op2;
	string op3;
	string op4;
	vector<Question>* questions= (vector<Question>*)output;

	if (argc == PARAMS_QUESTION_VARS)
	{
		returnVal = SQLITE_OK;
		// getting question details from arguments
		id = atoi(argv[INDEX_QUESTION_ID]);
		ques = argv[INDEX_QUESTION];
		correct = argv[INDEX_CORRECT_ANS];
		op2 = argv[INDEX_ANS_1];
		op3 = argv[INDEX_ANS_2];
		op4 = argv[INDEX_ANS_3];
	}
	// adding question to vector
	Question toIns(id, ques, correct, op2, op3, op4);
	questions->push_back(toIns);
	return returnVal;
}

/// <summary> callback for getting the current id count for the added game </summary>
int DataBase::callbackCount(void* output, int argc, char** argv, char** azColName)
{
	int returnVal = SQLITE_ERROR;
	// checking the arguments are sufficcent
	if (argv != nullptr && argc == 1)
	{
		// trying to get the number from the argument
		try
		{
			(*(int*)output) = atoi(argv[0]); // setting the game id to the argument's value
			returnVal = SQLITE_OK; 
		}
		catch (...){}
	}
	return returnVal;
}

/// <summary> callback marking if it was called through the boolean </summary>
int DataBase::callbackIsCalled(void* output, int argc, char** argv, char** azColName)
{
	(*(bool*)output) = true; // marks that a result was found
	return SQLITE_OK;
}

/// <sumamry> callback adding the given score to the bestscore in the additional parameter </summary>
int DataBase::callbackBestScores(void* scoreVec, int argc, char** argv, char** azColName)
{
	vector<string>* bestScores = (vector<string>*)scoreVec;
	if (argc == PARAMS_BEST_SCORES) // checking if sufficcent parameters were passed
	{
		// addings username and user's correct answers
		bestScores->push_back(argv[INDEX_USERNAME]);
		bestScores->push_back(argv[INDEX_WINS]);
	}
	return SQLITE_OK;
}

/// <summary> callback getting info about user, adding to given vector </summary>
int DataBase::callbackPersonalStatus(void* infoVec, int argc, char** argv, char** azColName)
{
	vector<string>* info = (vector<string>*)infoVec;
	// checking if function passed sufficent paramateres
	if (argc == PARAMS_PERSONAL_STATUS)
	{
		if (argv == nullptr || argv[0][0] == '0')
		{
			for (int i = 0; i < PARAMS_PERSONAL_STATUS; i++)
			{
				info->push_back("0");
			}
		}
		else
		{
			// adding all data, already sorted to be added in loop
			for (int i = 0; i < PARAMS_PERSONAL_STATUS; i++)
			{
				info->push_back(argv[i]);
			}
		}
	}
	return SQLITE_OK;
}

