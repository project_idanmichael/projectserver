#include "Room.h"
#include "Helper.h"
#include <iterator>
#define BYTE_LEN 2


// c'tors and d'tors

Room::Room(int id, User* admin, string roomName , int maxUsers, int questionTime, int questionNo)
{
	this->_ID = id;
	this->_admin = admin;
	this->_name = roomName;
	this->_maxUsers = maxUsers;
	this->_questionTime = questionTime;
	this->_questionNo = questionNo;
}

Room::~Room()
{
}

///<summarey>Tries to enter the user into the room, returns if succeeded</summary>
bool Room::joinRoom(User* user)
{
	bool canJoin = false;
	string msg;
	// checking if the room is full
	if ((int)this->_users.size() < this->_maxUsers)
	{
		canJoin = true;
		this->_users.push_back(user);
	}
	return canJoin;
}

///<summary>removes the given user from the room</summary>
void Room::leaveRoom(User* user)
{
	vector<User*>::iterator it;
	
	// checking if to close the room as well
	for (it = this->_users.begin(); it != this->_users.end(); it++)
	{
		if ((*it)->getUserName() == user->getUserName())
		{
			Helper::sendData((*it)->getSocket(), "1120"); // P_SEND_LEAVE_ROOM SUCCESS
			this->_users.erase(it);
			break;
		}
	}
}

///<summary>checks if user owns the room, if so closes the room - returns the room id if closed and -1 if not</summary>
int Room::closeRoom(User* user)
{
	int roomId = -1;
	if (user == this->_admin)
	{
		roomId = this->_ID;
		this->sendMessage("116"); // P_SEND_CLOSE_ROOM
		for (unsigned int i = 0; i < this->_users.size(); i++)
		{
			this->_users[i]->clearRoom();
		}
	}
	return roomId;
}

///<summaryreturns the users' list</summary>
vector<User*> Room::getUsers()
{
	return this->_users;
}

///<summary>returns the message of the user list request</summary>
string Room::getUsersListMessage()
{
	// variables
	string res = "108" + to_string(this->_users.size());
	string name;
	vector<User*>::iterator curr;
	// iterating the users and adds to string
	for (curr = this->_users.begin(); curr != this->_users.end(); ++curr)
	{
		name = (*curr)->getUserName();
		res += Helper::getPaddedNumber(name.length(), BYTE_LEN);
		res += name;
	}
	return res;
}

///<summary>returns the question's number</summary>
int Room::getQuestionsNo()
{
	return this->_questionNo;
}
///<summary>returns room id</summary>
int Room::getID()
{
	return this->_ID;
}
///<summary>returns room time</summary>
int Room::getQuestionTime()
{
	return this->_questionTime;
}

///<summary>returns the name of the room</summary>
string Room::getName()
{
	return this->_name;
}

///<summary>sends the given message to all participants in the room excluding the requested user</summary>
void Room::sendMessage(User* excludeUser, string message)
{
	vector<User*>::iterator curr;
	for (curr = this->_users.begin(); curr != this->_users.end(); ++curr)
	{
		if (*curr != excludeUser)
		{
			try
			{
				(*curr)->send(message);
			}
			catch (...)
			{
				cout << "failed sending message to " << (*curr)->getUserName() << endl;
			}
		}
	}
}

///<summary>sends the given message to all participants in the room</summary>
void Room::sendMessage(string message)
{
	this->sendMessage(nullptr, message);
}