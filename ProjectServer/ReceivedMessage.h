#pragma once
#include <string>
#include <iostream>
#include <vector>
#include <WinSock2.h>
#include "User.h"
using namespace std;
class ReceivedMessage
{
	public:
		ReceivedMessage(SOCKET, int, byte*, byte*);
		ReceivedMessage(SOCKET, int, byte*, byte*, vector<string>);
		SOCKET getSock();
		User* getUser();
		void setUser(User*);
		int getMessageCode();
		vector<string>& getValues();
		void closeSocket();
		byte* getKey();
		byte* getIV();

	private:
		byte* _iv;
		byte* _key;
		SOCKET _sock;
		User* _user;
		int _messageCode;
		vector<string> _values;
};
