#pragma once

#define P_SIGN_IN 200 /* 200<UsernameSize:2><Username><PasswordSize:2><Password> */
#define P_SIGN_OUT 201
#define P_SIGN_IN_REPLY 102
#define P_SIGN_UP 203 /* 203<UsernameSize:2><Username><PasswordSize:2><Password><EmailSize:2><Email> */
#define P_SIGN_UP_REPLY 104
#define P_REQ_ROOM_LIST 205
#define P_SEND_ROOM_LIST 106 /* 106<NumberOfRooms:4>||<RoomID:4><NameSize:2><Name>|| */
#define P_REQ_ROOM_USER_LIST 207 /* 207<RoomID:4> */
#define P_SEND_ROOM_USER_LIST 108 /* 108<NumberOfUsers:1>||<UsernameSize:2><Username>|| */
#define P_REQ_JOIN_ROOM 209 /* 209<RoomID:4> */
#define P_SEND_JOIN_ROOM 110 /* 110<JoinSuccessCode:1><QuestionCount:2><QuestionTime:2> */
#define P_REQ_LEAVE_ROOM 211
#define P_SEND_LEAVE_ROOM 112 /* (If server no longer exists since game has started or room was closed, message will not be sent) */
#define P_REQ_CREATE_ROOM 213 /* (number of players is char) 213<RoomNameSize:2><RoomName><PlayerCount><QuestionCount:2><QuestionTime:2> */
#define P_SEND_CREATE_ROOM 114 /* 114<CreateSuccessCode:1> */
#define P_REQ_CLOSE_ROOM 215
#define P_SEND_CLOSE_ROOM 116 /* Will be sent to all in room */
#define P_START_GAME 217
#define P_SEND_QUESTIONS 118 /* 118<QuestionSize:3><Question>||<AnswerSize:3><Answer> x 4|| */
#define P_REQ_CLIENT_ANSWER 219 /* 219<AnswerNumber:1><QuestionTime:2> (If client doesn't answer in time - answer is 5)*/
#define P_SEND_CLIENT_ANSWER 120 /* 120<AnswerCorrect:1> (1 - true, 0 - false) */
#define P_END_GAME 121
#define P_LEAVE_GAME 222
#define P_REQ_BEST_SCOERS 223
#define P_SEND_BEST_SCORES 124 /* 124||<Username_Size:2><Username><CorrectAnswerCount:6> x 3|| */
#define P_REQ_PERSONAL_STATUS 225
#define P_SEND_PERSONAL_STATUS 126 /* 126<GameCount:4><CorrectAnswers:6><WrongAnswers:6><AvgAnswerTime:4> */
#define P_CLIENT_EXIT 227
#define P_EXIT 299
#define P_INIT_CRYPT 666

#define SIZE_DETAIL 2
#define SIZE_USER_NAME 2
#define SIZE_PASSWORD 2
#define SIZE_EMAIL 2
#define SIZE_ROOM_ID 4
#define SIZE_ROOM_COUNT 4
#define SIZE_ROOM_USER_COUNT 1
#define SIZE_ROOM_NAME 2
#define SIZE_QUESTION_COUNT 2
#define SIZE_QUESTION_TIME 2
#define SIZE_QUESTION 3
#define SIZE_ANSWER 3
#define SIZE_ANSWER_NUMBER 1
#define SIZE_CORRECT_ANSWERS 6
#define SIZE_PERSONAL_GAMES 4
#define SIZE_WRONG_ANSWERS 6
#define SIZE_AVG_ANSWER_TIME 4 /* 2 bytes for number 2 bytes for after point */
#define SIZE_KEY_IV_CRYPT 16
#define COUNT_BEST_SCORES 3

#define CODE_SUCCESS '0'
#define CODE_ROOM_FULL '1'
#define CODE_ROOM_OTHER '2' /* Doesn't exist or other */
#define CODE_CREATE_FAIL '1'
#define CODE_SIGN_IN_WRONG '1'
#define CODE_SIGN_IN_CONNECTED '2'
#define P_SIGN_UP_BAD_PASS '1'
#define P_SIGN_UP_USER_EXIST '2'
#define P_SIGN_UP_BAD_USER '3'
#define P_SIGN_UP_OTHER '4'
