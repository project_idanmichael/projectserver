#include "ReceivedMessage.h"

/* 'tors */
ReceivedMessage::ReceivedMessage(SOCKET sock, int msgID, byte* key, byte* iv)
{
	this->_sock = sock;
	this->_messageCode = msgID;
	this->_key = key;
	this->_iv = iv;
}
ReceivedMessage::ReceivedMessage(SOCKET sock, int msgID, byte* key, byte* iv, vector<string> values) : ReceivedMessage(sock, msgID, key, iv)
{
	this->_values = values;
}

SOCKET ReceivedMessage::getSock()
{
	return this->_sock;
}
///<summary>Returns the user who sent the message</summary>
User* ReceivedMessage::getUser()
{
	return this->_user;
}

///<summary>Sets the message sender</summary>
void ReceivedMessage::setUser(User* user)
{
	//// maybe free previous user
	this->_user = user;
}

///<summary>returns the message's code</summary>
int ReceivedMessage::getMessageCode()
{
	return this->_messageCode;
}

///<summary>returns the parameters of the message code</summary>
vector<string>& ReceivedMessage::getValues()
{
	return this->_values;
}

///<summary>returns the key of the AES encryption</summary>
byte* ReceivedMessage::getKey()
{
	return this->_key;
}

///<summary>returns the iv of the AES encryption</summary>
byte* ReceivedMessage::getIV()
{
	return this->_iv;
}
