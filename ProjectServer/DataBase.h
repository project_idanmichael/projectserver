#pragma once
#include <string>
#include <iostream>
#include <vector>
#include "Game.h"
#include "Question.h"
#include "sqlite3.h"

#define FILE_NAME "trivia.db"
#define PARAMS_BEST_SCORES 2
#define INDEX_USERNAME 0
#define INDEX_WINS 1
#define COUNT_PAR
#define PARAMS_PERSONAL_STATUS 4
#define INDEX_QUESTION_ID 0
#define INDEX_QUESTION 1
#define INDEX_CORRECT_ANS 2
#define INDEX_ANS_1 3
#define INDEX_ANS_2 4
#define INDEX_ANS_3 5
#define PARAMS_BEST_SCORES 2
#define INDEX_WINS 1
#define INDEX_CORRECT_ANS 2
#define INDEX_WRONG_ANS 3
#define PARAMS_PERSONAL_STATUS 4
#define INDEX_GAME_COUNT 0
#define INDEX_CORRECT_ANS_COUNT 1
#define INDEX_WRONG_ANS_COUNT 2
#define INDEX_AVERAGE_TIME_ANS 3
#define PARAMS_QUESTION_VARS 6

using namespace std;

class Game;

class DataBase
{
	public:
		DataBase();
		~DataBase();
		bool isUserExists(string);
		bool addNewUser(string, string, string);
		bool isUserAndPassMatch(string, string);
		vector<Question> initQuestion(int); //--
		vector<string> getBestScores();
		vector<string> getPersonalStatus(string);
		int insertNewGame(); //--
		bool updateGameStatus(int); //--
		bool addAnswerToPlayer(int, string, int, string, bool, int); //--

		static vector<Game*> games;
		static vector<map<int, string[]>> questions;

	private:
		static int callbackCount(void*, int, char**, char**);
		static int callbackQuestions(void*, int, char**, char**);
		static int callbackBestScores(void*, int, char**, char**);
		static int callbackPersonalStatus(void*, int, char**, char**);
		static int callbackIsCalled(void*, int, char**, char**);

		sqlite3* _db;
};